import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import fullCalendar from 'vue-fullcalendar'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('full-calendar', fullCalendar)

require('./store/subscribe')

//CSS
import './assets/styles/_variables.scss'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/styles/_side-bar.scss'
import './assets/styles/_sidebar-themes.scss'
import './assets/styles/_custom.scss'
import './assets/styles/_common.scss'
import './assets/styles/_autocomplete.scss'
import './assets/styles/_side-header-switch.scss'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'

const local_api = 'http://localhost/acts-web-service/public/api/'
axios.defaults.baseURL = local_api

// const staging_api = 'http://staging.abcsystems.com.ph/acts-web-service/public/api/'
// axios.defaults.baseURL = staging_api
Vue.config.productionTip = false

store.dispatch('auth/attempt',localStorage.getItem('token')).then(() => {
    // store.dispatch('changeDefaultSystem',localStorage.getItem('active_system')).then(() =>{
    //
    // })
    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#app')
})

