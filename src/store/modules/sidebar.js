import axios from 'axios';
import router from '../../router/index';

export default {
    state:{
        systems: [],
        active_system: {},
        active_view:"",
        loading: true,
        loader:true
    },
    mutations:{
        setSystems: (state, systems) => (state.systems = systems),
        setActiveSystem: (state, system) => (state.active_system = system),
        setActiveView: (state, active) => (state.active_view = active),
        setLoading: (state, load) => (state.loading = load),
        setLoader: (state, loader) => (state.loader = loader)

    },
    getters:{
        allSystems: (state) => state.systems,
        chosenSystem: (state) => state.active_system,
        activeModule: (state) => state.active_system.modules,
        activeView: (state) => state.active_view,
        loading:(state) => state.loading,
        loader:(state) => state.loader
    },
    actions:{
        async fetchSystems({commit, dispatch}){
            const response = await axios.get('systems/all');
            await commit('setSystems', response.data);
            dispatch('fetchActiveSystem', localStorage.getItem("activeSystem"));
        },
        async fetchActiveSystem({commit, state, rootState}, system_id = null){
            let active = {};
            if(system_id != null){
                localStorage.setItem("activeSystem", system_id);
                active = state.systems.find(k => k.id==system_id);
            }else{
                if(rootState.auth.user.id == 1){
                    active = state.systems.find(k => k.id==5);
                    localStorage.setItem("activeSystem", 5);
                } else {
                    active = state.systems[0];
                    localStorage.setItem("activeSystem", state.systems[0].id);
                }
            }
            commit('setActiveSystem', active)
        },
        async fetchActiveView({state, commit}){
            let module = state.active_system.modules.find(k => k.sub_modules.length>0);
            let sub_module = module.sub_modules.find(k => k.route != "None");
            router.push({
                name: sub_module.route
            });
            commit('setActiveView', sub_module.route);
             
        },
        async fetchLoading({commit}, load){
           commit('setLoading', load);
        },
        async fetchLoader({commit}, loader){
           commit('setLoader', loader);
        }
    }
}