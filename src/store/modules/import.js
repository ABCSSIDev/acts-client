
export default {
    state: {
        imported_module: {},
        route_data: {}
    },
    mutations: {
        setImportModule: (state, module) => (state.imported_module = module),
        setRouteData: (state, data) => (state.route_data = data ),
    },
    getters: {
        importedModule: (state) => state.imported_module,
        setRouteData: (state) => state.route_data
    },
    actions: {
        async fetchModule({commit}, module){
            commit("setImportModule", module);
        },
        async fetchRouteData({commit}, route_data){
            commit("setRouteData", route_data);
        }
    }
}