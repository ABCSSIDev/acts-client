import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import sidebar from './modules/sidebar'
import import_fields from './modules/import'
import createPersistedState from "vuex-persistedstate";
import Payruns from './hris/payruns';
import DailyTimeRecord from './hris/daily_time_record';
import ThirteenthMonth from './hris/thirteenth_month';
import EmployeeSalary from './hris/employee_salary';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      open: false,
      toggle: false,
      notif: false,
      lazyload: false,
      appendEvent: [{
          benefits: [],
          delete: []
      }]
  },
  getters: {
      toggle: (state) => {
          return state.toggle
      },
      openState: (state) => {
        return state.open
      },
      notifState: (state) => {
        return state.notif
      },
      getLazyload: (state) => {
          return state.lazyload
      },
      getAppendedEvent: (state) => {
          return state.appendEvent
      }
  },
  mutations: {
      SET_TOGGLE: (state,newValue) => {
          state.toggle = newValue
      },
      SET_OPEN : (state,newValue) => {
        state.open = newValue
      },
      SET_NOTIF : (state,newValue) => {
        state.notif = newValue
      },
      SET_LAZY_LOAD: (state,newValue) => {
          state.lazyload = newValue
      },
      SET_APPEND_EVENT: (state,newValue) => {
          state.appendEvent = newValue
      }
  },
  actions: {
    setToggle: ({ commit, state },newValue) => {
      commit('SET_TOGGLE',newValue)
      return state.toggle
    },
    setOpen: ({commit, state},newValue) => {
        commit('SET_OPEN',newValue)
        return state.open
    },
    setNotif: ({commit, state},newValue) => {
      commit('SET_NOTIF',newValue)
      return state.notif
  },
    setAppendEvent: ({commit},newValue) => {
        commit('SET_APPEND_EVENT',newValue)
    }
  },
  modules: {
    auth,
    sidebar,
    import_fields,
    Payruns,
    DailyTimeRecord,
    ThirteenthMonth,
    EmployeeSalary
  },
  plugins: [createPersistedState()]
})
