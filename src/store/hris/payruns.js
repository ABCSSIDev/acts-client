import axios from 'axios'

export default {
    state: {
        payruns: [],
        history: []
    },
    mutations: {
        SET_PAYRUNS(state, payruns) {
            state.payruns = payruns
        },
        SET_HISTORY(state, history) {
            state.history = history
        }
    },
    getters: {
        payrunsList(state) {
            return state.payruns
        },
        historyList(state) {
            return state.history
        }
    },
    actions: {
        async fetchPayruns({commit}) {
            const response = await axios.get('payruns');
            await commit('SET_PAYRUNS', response.data)
            return response.data;
        },
        async fetchHistory({commit}) {
            const response = await axios.get('payruns/history');
            await commit('SET_HISTORY', response.data)
            return response.data;
        }
    }
}