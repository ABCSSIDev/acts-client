import axios from 'axios'

export default {
    state: {
        daily_time_records: [],
    },
    mutations: {
        SET_DAILY_TIME_RECORDS(state, daily_time_records) {
            state.daily_time_records = daily_time_records
        },
    },
    getters: {
        dailyTimeRecordList(state) {
            return state.daily_time_records
        }
    },
    actions: {
        async fetchDailyTimeRecords({commit}) {
            const response = await axios.get('daily_time_record');
            await commit('SET_DAILY_TIME_RECORDS', response.data)
            return response.data;
        }
    }
}