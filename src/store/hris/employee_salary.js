import axios from 'axios'

export default {
    state: {
        employee_salary: [],
    },
    mutations: {
        SET_EMPLOYEE_SALARY(state, employee_salary) {
            state.employee_salary = employee_salary
        },
    },
    getters: {
        employeeSalaryList(state) {
            return state.employee_salary
        }
    },
    actions: {
        async fetchEmployeeSalary({commit}) {
            const response = await axios.get('employee-salary');
            await commit('SET_EMPLOYEE_SALARY', response.data)
            return response.data;
        }
    }
}