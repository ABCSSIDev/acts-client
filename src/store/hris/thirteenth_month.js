import axios from 'axios'

export default {
    state: {
        thirteenth_months: [],
        thirteenth_month_history: []
    },
    mutations: {
        SET_13THMONTHS(state, thirteenth_months) {
            state.thirteenth_months = thirteenth_months
        },
        SET_THIRTEENTH_MONTH_HISTORY(state, thirteenth_month_history) {
            state.thirteenth_month_history = thirteenth_month_history
        }
    },
    getters: {
        thirteenthMonthList(state) {
            return state.thirteenth_months
        },
        thirteenthMonthHistoryList(state) {
            return state.thirteenth_month_history
        }
    },
    actions: {
        async fetchThirteenthMonth({commit}) {
            const response = await axios.get('thirteenthmonth');
            await commit('SET_13THMONTHS', response.data)
            return response.data;
        },
        async fetchThirteenthMonthHistory({commit}) {
            const response = await axios.get('thirteenthmonth/history');
            await commit('SET_THIRTEENTH_MONTH_HISTORY', response.data)
            return response.data;
        }
    }
}