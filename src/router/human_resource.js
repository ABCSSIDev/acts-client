import HRDashboard from '../views/human_resources/Dashboard'

//Employees
import EmployeeIndex from '../views/human_resources/organization/employees/Index.vue'
import EmployeeView from '../views/human_resources/organization/employees/Show.vue'
import EmployeeEdit from '../views/human_resources/organization/employees/Edit.vue'
import EmployeeCreate from '../views/human_resources/organization/employees/Create.vue'

//Department
import DepartmentIndex from '../views/human_resources/organization/department/Index.vue'
import DepartmentView from '../views/human_resources/organization/department/Show.vue'
import DepartmentCreate from '../views/human_resources/organization/department/Create.vue'
import DepartmentEdit from '../views/human_resources/organization/department/Edit.vue'

//Designation
import DesignationIndex from '../views/human_resources/organization/designation/Index.vue'
import DesignationView from '../views/human_resources/organization/designation/Show.vue'
import DesignationCreate from '../views/human_resources/organization/designation/Create.vue'
import DesignationEdit from '../views/human_resources/organization/designation/Edit.vue'

//Employee Exits
import EmployeeExitsIndex from '../views/human_resources/organization/employee_exits/Index.vue'
import EmployeeExitView from '../views/human_resources/organization/employee_exits/Show.vue'
import EmployeeExitsCreate from '../views/human_resources/organization/employee_exits/Create.vue'
import EmployeeExitEdit from '../views/human_resources/organization/employee_exits/Edit.vue'

//Org Chart
import OrgTreeIndex from '../views/human_resources/organization/org_tree/Index.vue'

//Leave
import LeaveTrackerIndex from '../views/human_resources/time_attendance/leave_tracker/Index.vue'
import LeaveTrackerView from '../views/human_resources/time_attendance/leave_tracker/Show.vue'
import LeaveTrackerCreate from '../views/human_resources/time_attendance/leave_tracker/Create.vue'
import LeaveTrackerEdit from '../views/human_resources/time_attendance/leave_tracker/Edit.vue'
import LeaveSettings from '../views/human_resources/time_attendance/leave_tracker/Settings.vue'
import LeaveTypeView from '../views/human_resources/time_attendance/leave_tracker/View.vue'

//Attendance
import EmployeeAttendanceIndex from '../views/human_resources/time_attendance/employee_attendance/Index.vue'
import EmployeeAttendanceView from '../views/human_resources/time_attendance/employee_attendance/Show.vue'

//Overtime
import OvertimeIndex from '../views/human_resources/time_attendance/overtime/Index.vue'
import OvertimeView from '../views/human_resources/time_attendance/overtime/Show.vue'
import OvertimeCreate from '../views/human_resources/time_attendance/overtime/Create.vue'
import OvertimeEdit from '../views/human_resources/time_attendance/overtime/Edit.vue'

//Timesheet
import TimesheetIndex from '../views/human_resources/time_attendance/timesheet/Index.vue'
import TimesheetView from '../views/human_resources/time_attendance/timesheet/Show.vue'
import TimesheetCreate from '../views/human_resources/time_attendance/timesheet/Create.vue'
import TimesheetEdit from '../views/human_resources/time_attendance/timesheet/Edit.vue'

//Official Business
import OfficialBusinessIndex from '../views/human_resources/time_attendance/official_business/Index.vue'
import OfficialBusinessView from '../views/human_resources/time_attendance/official_business/Show.vue'
import OfficialBusinessCreate from '../views/human_resources/time_attendance/official_business/Create.vue'
import OfficialBusinessEdit from '../views/human_resources/time_attendance/official_business/Edit.vue'

//Shift Schedule
import ShiftScheduleIndex from '../views/human_resources/time_attendance/shift_schedules/Index.vue'
import ShiftScheduleView from '../views/human_resources/time_attendance/shift_schedules/Show.vue'
import ShiftScheduleCreate from '../views/human_resources/time_attendance/shift_schedules/Create.vue'
import ShiftScheduleEdit from '../views/human_resources/time_attendance/shift_schedules/Edit.vue'

//13th Month
import ThirteenthMonthIndex from '../views/human_resources/payroll/thirteenth_months/Index.vue'
import ThirteenthMonthView from '../views/human_resources/payroll/thirteenth_months/Show.vue'
import ThirteenthMonthCreate from '../views/human_resources/payroll/thirteenth_months/Create.vue'
import ThirteenthMonthEdit from '../views/human_resources/payroll/thirteenth_months/Edit.vue'

//Payruns

import PayRunsIndex from '../views/human_resources/payroll/pay_runs/Index.vue'
import PayRunsView from '../views/human_resources/payroll/pay_runs/Show.vue'
import PayRunsCreate from '../views/human_resources/payroll/pay_runs/Create.vue'

//Employee Salary
import EmployeeSalariesIndex from '../views/human_resources/payroll/employee_salaries/Index.vue'
import EmployeeSalariesView from '../views/human_resources/payroll/employee_salaries/Show.vue'
import EmployeeSalariesEdit from '../views/human_resources/payroll/employee_salaries/Edit.vue'
import EmployeeSalariesCreate from '../views/human_resources/payroll/employee_salaries/Create.vue'

//Adjustments
import AdjustmentsIndex from '../views/human_resources/payroll/adjustments/Index.vue'
import AdjustmentView from '../views/human_resources/payroll/adjustments/Show.vue'
import AdjustmentsCreate from '../views/human_resources/payroll/adjustments/Create.vue'
import AdjustmentEdit from '../views/human_resources/payroll/adjustments/Edit.vue'

//Deductions
import DeductionIndex from '../views/human_resources/payroll/deductions/Index.vue'
import DeductionView from '../views/human_resources/payroll/deductions/Show.vue'
import DeductionCreate from '../views/human_resources/payroll/deductions/Create.vue'
import DeductionEdit from '../views/human_resources/payroll/deductions/Edit.vue'

//Index
import ReimbursementsIndex from '../views/human_resources/payroll/reimbursements/Index.vue'
import JobOpeningsIndex from '../views/human_resources/recruitment/job_openings/Index.vue'
import CandidatesIndex from '../views/human_resources/recruitment/candidates/Index.vue'
import InterviewsIndex from '../views/human_resources/recruitment/interviews/Index.vue'
import OnBoardingIndex from '../views/human_resources/recruitment/onboarding/Index.vue'
import ContributionIndex from '../views/human_resources/payroll/contribution/Index.vue'
import DTRIndex from '../views/human_resources/time_attendance/daily_time_record/Index.vue'

//View
import ReimbursementView from '../views/human_resources/payroll/reimbursements/Show.vue'
import JobOpeningsView from '../views/human_resources/recruitment/job_openings/Show.vue'
import CandidateView from '../views/human_resources/recruitment/candidates/Show.vue'
import InterviewView from '../views/human_resources/recruitment/interviews/Show.vue'
import OnBoardingView from '../views/human_resources/recruitment/onboarding/Show.vue'
import DTRShow from '../views/human_resources/time_attendance/daily_time_record/Show.vue'

//Create
import LeaveTypeCreate from '../views/human_resources/time_attendance/leave_tracker/New.vue'
import ReimbursementCreate from '../views/human_resources/payroll/reimbursements/Create.vue'
import JobOpeningsCreate from '../views/human_resources/recruitment/job_openings/Create.vue'
import CandidatesCreate from '../views/human_resources/recruitment/candidates/Create.vue'
import InterviewsCreate from '../views/human_resources/recruitment/interviews/Create.vue'
import OnBoardingCreate from '../views/human_resources/recruitment/onboarding/Create.vue'
import OnBoardingConvert from '../views/human_resources/recruitment/onboarding/Convert.vue'
import SssCreate from '../views/human_resources/payroll/contribution/SssCreate.vue'
import PagibigCreate from '../views/human_resources/payroll/contribution/PagibigCreate.vue'
import PhilhealthCreate from '../views/human_resources/payroll/contribution/PhilhealthCreate.vue'

// Edit
import JobOpeningsEdit from '../views/human_resources/recruitment/job_openings/Edit.vue'
import CandidateEdit from '../views/human_resources/recruitment/candidates/Edit.vue'
import InterviewEdit from '../views/human_resources/recruitment/interviews/Edit.vue'
import OnboardingEdit from '../views/human_resources/recruitment/onboarding/Edit.vue'


//Reports
import KawaniReportsIndex from '../views/human_resources/reports/reports_list.vue'
import KawaniReportsEmployee from '../views/human_resources/reports/reports_employee.vue'
import KawaniReportsLeave from '../views/human_resources/reports/reports_leave.vue'
import KawaniReportsOvertime from '../views/human_resources/reports/reports_overtime.vue'
import KawaniReportsAttendance from '../views/human_resources/reports/reports_attendance.vue'
import KawaniReportsBirthday from '../views/human_resources/reports/reports_birthday.vue'
import KawaniReportsTimesheet from '../views/human_resources/reports/reports_timesheet.vue'
import KawaniReportsEmployeeExit from '../views/human_resources/reports/reports_exit.vue'
import KawaniReportsShiftSchedule from '../views/human_resources/reports/reports_schedule.vue'
import KawaniReportsPayroll from '../views/human_resources/reports/reports_payroll.vue'

import MyPortalIndex from '../views/human_resources/my_portal'
import ActivitiesView from '../views/human_resources/Activities.vue'
import BiometricsImport from '../views/human_resources/time_attendance/employee_attendance/Import.vue'
import NotificationIndex from '../views/human_resources/notifications.vue'

import Timeline from '../components/Timeline'
import ImportPage from '@/components/ImportPage'

let human_routes = [
    {
        path: 'activities',
        name: 'human_resource.activities',
        component: ActivitiesView
    },
    {
        path: '',
        name: 'human.home',
        component: HRDashboard
    },
    // import 
    {
        path: '/human_resource/employee_exits/import/:import_data',
        name: 'employee_exits.import',
        component: ImportPage
    },
    {
        path: '/human_resource/employees/import/:import_data',
        name: 'employees.import',
        component: ImportPage
    },
    {
        path: 'employee-attendance/import',
        name: 'employee_attendance.import',
        component: BiometricsImport
    },
    // index
    {
        path: 'employees',
        name: 'employees',
        component: EmployeeIndex
    },
    {
        path: 'daily_time_record',
        name: 'daily_time_record',
        component: DTRIndex
    },
    {
        path: 'departments',
        name: 'departments',
        component: DepartmentIndex
    },
    {
        path: 'designations',
        name: 'designations',
        component: DesignationIndex
    },
    {
        path: 'employee-exits',
        name: 'employee_exits',
        component: EmployeeExitsIndex
    },
    {
        path: 'organizational-tree',
        name: 'org_tree',
        component: OrgTreeIndex
    },
    {
        path: 'employee-attendance',
        name: 'employee_attendance',
        component: EmployeeAttendanceIndex
    },
    {
        path: 'leave-trackers',
        name: 'leave_tracker',
        component: LeaveTrackerIndex
    },
    {
        path: 'overtime',
        name: 'overtime',
        component: OvertimeIndex
    },
    {
        path: 'timesheet',
        name: 'timesheet',
        component: TimesheetIndex
    },
    {
        path: 'official-business',
        name: 'official_business',
        component: OfficialBusinessIndex
    },
    {
        path: 'shift-schedules',
        name: 'shift_schedules',
        component: ShiftScheduleIndex
    },
    {
        path: 'pay-runs',
        name: 'pay_runs',
        component: PayRunsIndex
    },
    {
        path: 'contribution',
        name: 'contribution',
        component: ContributionIndex
    },
    {
        path: 'employee-salaries',
        name: 'employee_salaries',
        component: EmployeeSalariesIndex
    },
    {
        path: 'adjustments',
        name: 'adjustments',
        component: AdjustmentsIndex
    },
    {
        path: 'reimbursements',
        name: 'reimbursements',
        component: ReimbursementsIndex
    },
    {
        path: 'job-openings',
        name: 'job_openings',
        component: JobOpeningsIndex
    },
    {
        path: 'candidates',
        name: 'candidates',
        component: CandidatesIndex
    },
    {
        path: 'interviews',
        name: 'interviews',
        component: InterviewsIndex,
        meta: {
            module_id: 72
        }
    },
    {
        path: 'onboardings',
        name: 'onboarding',
        component: OnBoardingIndex
    },
    {
        path: 'notifications',
        name: 'notifications',
        component: NotificationIndex
    },
    {
        path: 'reports-list',
        name: 'human_resource.reports_list',
        component: KawaniReportsIndex
    },

    //create
    {
        path: '/human_resource/employees/create',
        name: 'employees.create',
        component: EmployeeCreate
    },
    {
        path: '/human_resource/departments/create',
        name: 'departments.create',
        component: DepartmentCreate
    },
    {
        path: '/human_resource/designations/create',
        name: 'designations.create',
        component: DesignationCreate
    },
    {
        path: '/human_resource/employee-exits/create',
        name: 'employee_exits.create',
        component: EmployeeExitsCreate
    },
    {
        path: '/human_resource/leave-tracker/create',
        name: 'leave_tracker.create',
        component: LeaveTrackerCreate
    },
    {
        path: '/human_resource/overtime/create',
        name: 'overtime.create',
        component: OvertimeCreate
    },
    {
        path: '/human_resource/timesheet/create',
        name: 'timesheet.create',
        component: TimesheetCreate
    },
    {
        path: '/human_resource/official-business/create',
        name: 'official_business.create',
        component: OfficialBusinessCreate
    },
    {
        path: '/human_resource/shift-schedules/create',
        name: 'shift_schedules.create',
        component: ShiftScheduleCreate
    },
    {
        path: '/human_resource/pay-runs/create',
        name: 'pay_runs.create',
        component: PayRunsCreate
    },
    {
        path: '/human_resource/employee-salaries/create',
        name: 'employee_salaries.create',
        component: EmployeeSalariesCreate
    },
    {
        path: '/human_resource/adjustments/create',
        name: 'adjustments.create',
        component: AdjustmentsCreate
    },
    {
        path: '/human_resource/reimbursements/create',
        name: 'reimbursements.create',
        component: ReimbursementCreate
    },
    {
        path: '/human_resource/job-openings/create',
        name: 'job_openings.create',
        component: JobOpeningsCreate
    },
    {
        path: '/human_resource/candidates/create',
        name: 'candidates.create',
        component: CandidatesCreate
    },
    {
        path: '/human_resource/interviews/create',
        name: 'interviews.create',
        component: InterviewsCreate
    },
    {
        path: '/human_resource/onboarding/create',
        name: 'onboarding.create',
        component: OnBoardingCreate
    },
    {
        path: '/human-resource/leave-trackers/settings/create',
        name: 'leave_tracker.settings.create',
        component: LeaveTypeCreate
    },
    {
        path: '/human_resource/onboarding/create',
        name: 'human_resource.onboarding.convert',
        component: OnBoardingConvert
    },
    {
        path: 'sss/create',
        name: 'sss.create',
        component: SssCreate
    },
    {
        path: 'pagibig/create',
        name: 'pagibig.create',
        component: PagibigCreate
    },
    {
        path: 'philhealth/create',
        name: 'philhealth.create',
        component: PhilhealthCreate
    },
    // edit
    {
        path: '/human_resource/departments/edit/:id',
        name: 'departments.edit',
        component: DepartmentEdit
    },
    {
        path: 'employee-exits/edit/:id',
        name: 'employee_exits.edit',
        component: EmployeeExitEdit
    },
    {
        path: 'overtime/edit/:id',
        name: 'overtime.edit',
        component: OvertimeEdit
    },
    {
        path: '/human_resource/designation/edit/:id',
        name: 'designations.edit',
        component: DesignationEdit 
    },
    {
        path: '/human_resource/employees/edit/:id',
        name: 'employees.edit',
        component: EmployeeEdit
    },
    {
        path: 'employee-salaries/edit/',
        name: 'employee_salaries.edit',
        component: EmployeeSalariesEdit
    },
    {
        path: 'leave-tracker/edit/:id',
        name: 'leave.tracker.edit',
        component: LeaveTrackerEdit
    },
    {
        path: 'timesheet/edit/:id',
        name: 'timesheet.edit',
        component: TimesheetEdit
    },
    {
        path: 'job-openings/edit/:id',
        name: 'job_openings.edit',
        component: JobOpeningsEdit
    },
    {
        path: 'shift-schedules/edit/:id',
        name: 'shift.schedule.edit',
        component: ShiftScheduleEdit
    },
    {
        path: 'official-business/edit/:id',
        name: 'official_business.edit',
        component: OfficialBusinessEdit
    },
    {
        path: 'adjustment/edit/:id',
        name: 'adjustment.edit',
        component: AdjustmentEdit
    },
    {
        path: 'candidates/edit/:id',
        name: 'candidate.edit',
        component: CandidateEdit
    },
    {
        path: 'interviews/edit/:id',
        name: 'interview.edit',
        component: InterviewEdit
    },
    {
        path: 'onboardings/edit/:id',
        name: 'onboarding.edit',
        component: OnboardingEdit
    },
    //view
    {
        path: 'daily_time_record/view/:id',
        name: 'daily_time_record.view',
        component: DTRShow
    },
    {
        path: 'employees/view/:id',
        name: 'employees.view',
        component: EmployeeView,
    },
    {
        path: 'departments/view/:id',
        name: 'departments.view',
        component: DepartmentView
    },
    {
        path: 'designations/view/:id',
        name: 'designations.view',
        component: DesignationView
    },
    {
        path: 'employee-exits/view/:id',
        name: 'employee_exits.view',
        component: EmployeeExitView
    },
    {
        path: 'employee-attendance/view/:id',
        name: 'employee_attendance.view',
        component: EmployeeAttendanceView
    },
    {
        path: 'leave-tracker/view/:id',
        name: 'leave_tracker.view',
        component: LeaveTrackerView
    },
    {
        path: 'overtime/view/:id',
        name: 'overtime.view',
        component: OvertimeView
    },
    {
        path: 'shift-schedules/view/:id',
        name: 'shift_schedules.view',
        component: ShiftScheduleView
    },
    {
        path: 'pay-runs/view/:id',
        name: 'pay_runs.view',
        component: PayRunsView
    },
    {
        path: 'employee-salaries/view/',
        name: 'employee_salaries.view',
        component: EmployeeSalariesView
    },
    {
        path: 'adjustments/view/:id',
        name: 'adjustments.view',
        component: AdjustmentView
    },
    {
        path: 'reimbursements/view/:id',
        name: 'reimbursements.view',
        component: ReimbursementView
    },
    {
        path: 'job-openings/view/:id',
        name: 'job_opening.view',
        component: JobOpeningsView
    },
    {
        path: 'timesheet/view/:id',
        name: 'timesheet.view',
        component: TimesheetView
    },    
    {
        path: 'official-business/view/:id',
        name: 'official_business.view',
        component: OfficialBusinessView
    },
    {
        path: 'candidates/view/:id',
        name: 'candidates.view',
        component: CandidateView
    },
    {
        path: 'interviews/view/:id',
        name: 'interview.view',
        component: InterviewView,
    },
    {
        path: 'onboardings/view/:id',
        name: 'onboarding.view',
        component: OnBoardingView
    },
    {
        path: 'my-portal/view',
        name: 'my_portal.view',
        component: MyPortalIndex
    },
    {
        path: 'leave-trackers/settings',
        name: 'leave_tracker.settings',
        component: LeaveSettings
    },
    {
        path: 'leave-trackers/settings/view/:id',
        name: 'leave_tracker.settings.view',
        component: LeaveTypeView
    },
    {
        path: 'employee-reports',
        name: 'human_resource.reports_employee',
        component: KawaniReportsEmployee
    },
    {
        path: 'leave-reports',
        name: 'human_resource.reports_leave',
        component: KawaniReportsLeave
    },
    {
        path: 'overtime-reports',
        name: 'human_resource.reports_overtime',
        component: KawaniReportsOvertime
    },
    {
        path: 'attendance-reports',
        name: 'human_resource.reports_attendance',
        component: KawaniReportsAttendance
    },
    {
        path: 'birthday-reports',
        name: 'human_resource.reports_birthday',
        component: KawaniReportsBirthday
    },
    {
        path: 'timesheet-reports',
        name: 'human_resource.reports_timesheet',
        component: KawaniReportsTimesheet
    },
    {
        path: 'employee-exit-reports',
        name: 'human_resource.reports_exit',
        component: KawaniReportsEmployeeExit
    },
    {
        path: 'shift-schedule-reports',
        name: 'human_resource.reports_schedule',
        component: KawaniReportsShiftSchedule
    },
    {
        path: 'payroll-reports',
        name: 'human_resource.reports_payroll',
        component: KawaniReportsPayroll
    },

    //timeline
    {
        path: 'employees/timeline/:id',
        name: 'employees.timeline',
        component: Timeline
    },
    {
        path: 'departments/timeline/:id',
        name: 'departments.timeline',
        component: Timeline
    },
    {
        path: 'designations/timeline/:id',
        name: 'designations.timeline',
        component: Timeline
    },
    {
        path: 'employee_exits/timeline/:id',
        name: 'employee_exits.timeline',
        component: Timeline
    },
    {
        path: 'leave_tracker/timeline/:id',
        name: 'leave_tracker.timeline',
        component: Timeline
    },
    {
        path: 'overtime/timeline/:id',
        name: 'overtime.timeline',
        component: Timeline
    },
    {
        path: 'timesheet/timeline/:id',
        name: 'timesheet.timeline',
        component: Timeline
    },
    {
        path: 'shift_schedules/timeline/:id',
        name: 'shift_schedules.timeline',
        component: Timeline
    },
    {
        path: 'adjustments/timeline/:id',
        name: 'adjustments.timeline',
        component: Timeline
    },
    {
        path: 'reimbursements/timeline/:id',
        name: 'reimbursements.timeline',
        component: Timeline
    },
    {
        path: 'job-openings/timeline/:id',
        name: 'job_opening.timeline',
        component: Timeline
    },
    {
        path: 'candidates/timeline/:id',
        name: 'candidates.timeline',
        component: Timeline
    },
    {
        path: 'interviews/timeline/:id',
        name: 'interviews.timeline',
        component: Timeline,
    },
    {
        path: 'onboardings/timeline/:id',
        name: 'onboardings.timeline',
        component: Timeline
    },
    //deduction routes
    {
        path: 'deductions',
        name: 'deductions',
        component: DeductionIndex
    },
    {
        path: 'deductions/view/:id',
        name: 'deductions.view',
        component: DeductionView
    },
    {
        path: '/human_resource/deductions/create',
        name: 'deductions.create',
        component: DeductionCreate
    },
    {
        path: 'deductions/edit/:id',
        name: 'deductions.edit',
        component: DeductionEdit
    },
    {
        path: 'thirteenth_months',
        name: 'thirteenth_months',
        component: ThirteenthMonthIndex
    },
    {
        path: 'thirteenth_months/view/:id',
        name: 'thirteenth_months.view',
        component: ThirteenthMonthView
    },
    {
        path: '/human_resource/thirteenth_months/create',
        name: 'thirteenth_months.create',
        component: ThirteenthMonthCreate
    },
    {
        path: 'thirteenth_months/edit/:id',
        name: 'thirteenth_months.edit',
        component: ThirteenthMonthEdit
    },
]

export default {
    human_routes: human_routes
}