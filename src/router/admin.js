import UserIndex from '../views/administrator/user/Index'
import UsersView from '../views/administrator/user/Show'
import UserCreate from '../views/administrator/user/Create'
import RolesIndex from '../views/administrator/roles/index'
import RolesCreate from '../views/administrator/roles/Create'
import ModulesIndex from '../views/administrator/modules/Index'
import ModulesShow from '../views/administrator/modules/Show'
import PermissionIndex from '../views/administrator/permission/Index'
import PermissionCreate from '../views/administrator/permission/Create'
import PermissionsShow from '../views/administrator/permission/Show'
import ApprovalProcessIndex from '../views/administrator/approval/Index.vue'
import ApprovalProcessView from '../views/administrator/approval/Show.vue'
import ApprovalProcessCreate from '../views/administrator/approval/Create.vue'
import AuditTrailsIndex from '../views/administrator/audit/Index.vue'
import RolesEdit from '../views/administrator/roles/Edit.vue'
import PermissionsEdit from '../views/administrator/permission/Edit.vue'
import UsersEdit from '../views/administrator/user/Edit.vue'

let admin_routes = [
    {
        path: '/',
        name: 'admin.home',
        component: UserIndex,
        beforeEnter: (to, from, next) => {
            console.log('middleware')
            next()
        },
    },
    {
        path: 'users',
        name: 'users',
        component: UserIndex
    },
    {
        path: 'users/view/:id',
        name: 'users.view',
        component: UsersView
    },
    {
        path: 'users/create',
        name: 'users.create',
        component: UserCreate
    },
    {
        path: 'permissions',
        name: 'permissions',
        component: PermissionIndex
    },
    {
        path: 'permissions/create',
        name: 'permissions.create',
        component: PermissionCreate
    },
    {
        path: 'roles',
        name: 'roles',
        component: RolesIndex
    },
    {
        path: 'roles/create/:id',
        name: 'roles.create',
        component: RolesCreate
    },
    {
        path: 'roles/edit/:id',
        name: 'roles.edit',
        component: RolesEdit
    },
    {
        path: 'modules',
        name: 'modules.index',
        component: ModulesIndex
    },
    {
        path: 'modules/:id',
        name: 'modules.show',
        component: ModulesShow
    },
    {
        path: 'permissions/:id',
        name: 'permissions.view',
        component: PermissionsShow
    },
    {
        path: 'approval-process',
        name: 'approval_process',
        component: ApprovalProcessIndex
    },
    {
        path: 'approval-process/view/:id',
        name: 'approval_process.view',
        component: ApprovalProcessView
    },
    {
        path: 'approval-process/create',
        name: 'approval_process.create',
        component: ApprovalProcessCreate
    },
    {
        path: 'audit-trails',
        name: 'audit_trails',
        component: AuditTrailsIndex
    },
    {
        path: '/adminstrator/users/edit/:id',
        name: 'users.edit',
        component: UsersEdit
    },
    {
        path: '/adminstrator/permission/edit/:id',
        name: 'permissions.edit',
        component: PermissionsEdit
    },
]

export default {
    admin_routes: admin_routes
}