//inventory modules

//index
//inventory modules

//index
import ReturnSlipIndex from '../views/inventory/operations/return_slip/Index.vue'
import TransmittalSlipIndex from '../views/inventory/operations/transmittal_slip/Index.vue'
import PremiseEquipmentIndex from '../views/inventory/asset/premise_equipment/Index.vue'
import SuppliesEquipmentIndex from '../views/inventory/asset/supplies_equipment/Index.vue'
import OperationMaterialsIndex from '../views/inventory/asset/operation_material/Index.vue'
import MaterialIssuanceSlipIndex from '../views/inventory/operations/material_issuance_slip/Index.vue'
import AccountabilityRequestIndex from '../views/inventory/operations/accountability_request/Index.vue'
import AccountabilityIssuanceIndex from '../views/inventory/operations/accountability_issuance/Index.vue'
import ReceivingIndex from '../views/inventory/asset/receiving/Index.vue'
import IncidentReportIndex from '../views/inventory/operations/incident_report/Index.vue'
import CategoryIndex from '../views/inventory/procurement/category/Index.vue'
import PurchaseRequestIndex from '../views/inventory/procurement/purchase_request/Index.vue'
import PurchaseOrderIndex from '../views/inventory/procurement/purchase_order/Index.vue'
import ServiceOrderIndex from '../views/inventory/operations/service_order/Index.vue'
import TeamIndex from '../views/inventory/operations/team/Index.vue'

//create
import ReceivingCreate from '../views/inventory/asset/receiving/Create.vue'
import ReturnSlipCreate from '../views/inventory/operations/return_slip/Create.vue'
import TransmittalSlipCreate from '../views/inventory/operations/transmittal_slip/Create.vue'
import AssetCreate from '../views/inventory/asset/supplies_equipment/assets/Create.vue'
import ConsumableCreate from '../views/inventory/asset/supplies_equipment/consumables/Create.vue'
import PremiseEquipmentCreate from '../views/inventory/asset/premise_equipment/Create.vue'
import OperationMaterialsCreate from '../views/inventory/asset/operation_material/Create.vue'
import MaterialIssuanceTemplateCreate from '../views/inventory/operations/material_issuance_slip/Create_Template.vue'
import MaterialIssuanceSlipCreate from '../views/inventory/operations/material_issuance_slip/Create.vue'
import AccountabilityRequestCreate from '../views/inventory/operations/accountability_request/Create.vue'
import AccountabilityIssuanceCreate from '../views/inventory/operations/accountability_issuance/Create.vue'
import NewFieldCreate from '../views/inventory/asset/operation_material/NewField.vue'
import NewSpecsCreate from '../views/inventory/asset/supplies_equipment/assets/NewField.vue'
import IncidentReportCreate from '../views/inventory/operations/incident_report/Create.vue'
import CategoryCreate from '../views/inventory/procurement/category/Create.vue'
import ServiceOrderCreate from '../views/inventory/operations/service_order/Create.vue'
import TeamCreate from '../views/inventory/operations/team/Create.vue'
import PurchaseRequestCreate from '../views/inventory/procurement/purchase_request/Create.vue'
import PurchaseOrderCreate from '../views/inventory/procurement/purchase_order/Create.vue'

// Edit
import CategoryEdit from '../views/inventory/procurement/category/Edit.vue'
import IncidentReportEdit from '../views/inventory/operations/incident_report/Edit.vue'
import AssetEdit from '../views/inventory/asset/supplies_equipment/assets/Edit.vue'
import PremiseEquipmentEdit from '../views/inventory/asset/premise_equipment/Edit.vue'

//view
import ReceivingView from '../views/inventory/asset/receiving/Show.vue'
import AssetView from '../views/inventory/asset/supplies_equipment/assets/Show.vue'
import ConsumableView from '../views/inventory/asset/supplies_equipment/consumables/Show.vue'
import ReturnSlipView from '../views/inventory/operations/return_slip/Show.vue'
import TransmittalSlipView from '../views/inventory/operations/transmittal_slip/Show.vue'
import PremiseEquipmentView from '../views/inventory/asset/premise_equipment/Show.vue'
import OperationMaterialsView from '../views/inventory/asset/operation_material/Show.vue'
import OperationMaterialsEdit from '../views/inventory/asset/operation_material/Edit.vue'
import MaterialIssuanceSlipView from '../views/inventory/operations/material_issuance_slip/Show.vue'
import AccountabilityRequestView from '../views/inventory/operations/accountability_request/Show.vue'
import AccountabilityIssuanceView from '../views/inventory/operations/accountability_issuance/Show.vue'
import IncidentReportView from '../views/inventory/operations/incident_report/Show.vue'
import CategoryView from '../views/inventory/procurement/category/Show.vue'
import ServiceOrderView from '../views/inventory/operations/service_order/Show.vue'
import TeamView from '../views/inventory/operations/team/Show.vue'
import PurchaseRequestView from '../views/inventory/procurement/purchase_request/Show.vue'
import ImportPage from '@/components/ImportPage'
import InventoryDashboard from '../views/inventory/Dashboard'

//reports
import KamaligAssetReports from '../views/inventory/reports/reports_assets.vue'
import KamaligAccountabilityReports from '../views/inventory/reports/reports_accountability.vue'

let inventory_routes = [
    {
        path: '',
        name: 'inventory.home',
        component: InventoryDashboard
    },

    //index
    {
        path: 'office-supplies-equipment',
        name: 'supplies_equipment',
        component: SuppliesEquipmentIndex
    },
    {
        path: 'customer-premise-equipment',
        name: 'premise_equipment',
        component: PremiseEquipmentIndex
    },
    {
        path: 'operation-material',
        name: 'operation_material',
        component: OperationMaterialsIndex 
    },
    {
        path: '/inventory/return-slip',
        name: 'return_slip',
        component: ReturnSlipIndex
    },
    {
        path: '/inventory/transmittal-slip',
        name: 'transmittal_slip',
        component: TransmittalSlipIndex
    },
    {
        path: 'material-issuance-slip',
        name: 'material_issuance_slip',
        component: MaterialIssuanceSlipIndex
    },
    {
        path: 'accountability-request',
        name: 'accountability_request',
        component: AccountabilityRequestIndex
    },
    {
        path: 'accountability-issuance',
        name: 'accountability_issuance',
        component: AccountabilityIssuanceIndex
    },
    {
        path: 'receiving',
        name: 'receiving',
        component: ReceivingIndex
    },
    {
        path: 'incident-report',
        name: 'incident_report',
        component: IncidentReportIndex
    },
    {
        path: 'category',
        name: 'category',
        component: CategoryIndex
    },
    {
        path: 'purchase-request',
        name: 'purchase_request',
        component: PurchaseRequestIndex
    },
    {
        path: 'purchase-order',
        name: 'purchase_order',
        component: PurchaseOrderIndex
    },
    {
        path: 'service-order',
        name: 'service_order',
        component: ServiceOrderIndex
    },
    {
        path: 'team',
        name: 'team',
        component: TeamIndex
    },

    //create
    {
        path: '/inventory/receiving/create',
        name: 'receiving.create',
        component: ReceivingCreate
    },
    {
        path: '/inventory/office-supplies-equipment/asset/create',
        name: 'supplies_equipment.create.assets',
        component: AssetCreate
    },
    {   path: '/inventory/office-supplies-equipment/new-field/create',
        name: 'new_field.create',
        component: NewSpecsCreate
    },
    {
        path: '/inventory/office-supplies-equipment/consumable/create',
        name: 'supplies_equipment.create.consumables',
        component: ConsumableCreate
    },
    {
        path: '/inventory/return-slip/create',
        name: 'return.slip.create',
        component: ReturnSlipCreate
    },
    {
        path: '/inventory/transmittal-slip/create',
        name: 'transmittal.slip.create',
        component: TransmittalSlipCreate
    },
    {
        path: '/inventory/customer-premise-equipment/create',
        name: 'premise_equipment.create',
        component: PremiseEquipmentCreate
    },
    {
        path: '/inventory/operation-material/create',
        name: 'operation_material.create',
        component: OperationMaterialsCreate
    },
    {
        path: '/inventory/material-issuance-slip/create',
        name: 'material_issuance_slip.create',
        component: MaterialIssuanceSlipCreate
    },
    {
        path: '/inventory/material-issuance-template/create',
        name: 'material_issuance_slip.create.template',
        component: MaterialIssuanceTemplateCreate
    },
    {
        path: '/inventory/accountability-request/create',
        name: 'accountability_request.create',
        component: AccountabilityRequestCreate
    },
    {
        path: '/inventory/accountability-issuance/create',
        name: 'accountability_issuance.create',
        component: AccountabilityIssuanceCreate
    },
    {   path: '/inventory/operation-material/new-field/create',
        name: 'new_field.create',
        component: NewFieldCreate
    },
    {
        path: '/inventory/incident-report/create',
        name: 'incident_report.create',
        component: IncidentReportCreate
    },
    {
        path: '/inventory/category/create',
        name: 'category.create',
        component: CategoryCreate
    },
    {
        path: '/inventory/purchase-request/create',
        name: 'purchase_request.create',
        component: PurchaseRequestCreate
    },
    {
        path: '/inventory/purchase-order/create',
        name: 'purchase_order.create',
        component: PurchaseOrderCreate
    },
    {
        path: '/inventory/service-order/create',
        name: 'service_order.create',
        component: ServiceOrderCreate
    },
    {
        path: '/inventory/team/create',
        name: 'team.create',
        component: TeamCreate
    },
    // edit 
    {
        path: '/inventory/category/edit/:id',
        name: 'category.edit',
        component: CategoryEdit
    },
    {
        path: '/inventory/incident_report/edit/:id',
        name: 'incident_report.edit',
        component: IncidentReportEdit
    },
    {
        path: '/inventory/office-supplies-equipment/asset/edit/:id',
        name: 'assets.edit',
        component: AssetEdit
    },
    {
        path: '/inventory/customer-premise-equipment/edit/:id',
        name: 'premise_equipment.edit',
        component: PremiseEquipmentEdit
    },
    //view
    {
        path: '/inventory/receiving/view/:id',
        name: 'receiving.view',
        component: ReceivingView
    },
    {
        path: '/inventory/office-supplies-equipment/asset/view/:id',
        name: 'supplies_equipment.view.assets',
        component: AssetView
    },
    {
        path: '/inventory/office-supplies-equipment/consumable/view/:id',
        name: 'supplies_equipment.view.consumables',
        component: ConsumableView
    },
    {
        path: '/inventory/return-slip/view/:id',
        name: 'return.slip.view',
        component: ReturnSlipView
    },
    {
        path: '/inventory/transmittal-slip/view/:id',
        name: 'transmittal.slip.view',
        component: TransmittalSlipView
    },
    {
        path: '/inventory/customer-premise-equipment/view/:id',
        name: 'premise_equipment.view',
        component: PremiseEquipmentView
    },
    {
        path: '/inventory/operation-material/view/:id',
        name: 'operation_material.view',
        component: OperationMaterialsView
    },
    {
        path: '/inventory/operation-material/edit/:id',
        name: 'operation_material.edit',
        component: OperationMaterialsEdit
    },
    {
        path: '/inventory/material-issuance-slip/view/:id',
        name: 'material_issuance_slip.view',
        component: MaterialIssuanceSlipView
    },
    {
        path: '/inventory/accountability-request/view/:id',
        name: 'accountability_request.view',
        component: AccountabilityRequestView
    },
    {
        path: '/inventory/accountability-issuance/view/:id',
        name: 'accountability_issuance.view',
        component: AccountabilityIssuanceView
    },
    {
        path: '/inventory/incident-report/view/:id',
        name: 'incident_report.view',
        component: IncidentReportView
    },
    {
        path: '/inventory/category/view/:id',
        name: 'category.view',
        component: CategoryView
    },
    {
        path: '/inventory/purchase-request/view/:id',
        name: 'purchase_request.view',
        component: PurchaseRequestView
    },
    {
        path: '/inventory/service-order/view/:id',
        name: 'service_order.view',
        component: ServiceOrderView
    },
    {
        path: '/inventory/team/view/:id',
        name: 'team.view',
        component: TeamView
    },
    //import
    {
        path: '/inventory/operation-material/import/',
        name: 'operation_material.import',
        component: ImportPage
    },
    {
        path: '/inventory/premise-equipment/import/:import_data',
        name: 'premise_equipment.import',
        component: ImportPage
    },
    {
        path: '/inventory/office-supplies-equipment/assets/import/',
        name: 'assets.import',
        component: ImportPage
    },
    {
        path: '/inventory/office-supplies-equipment/consumables/import/',
        name: 'consumables.import',
        component: ImportPage
    },
    //Reports
    {
        path: 'assets-reports',
        name: 'inventory.reports_assets',
        component: KamaligAssetReports
    },
    {
        path: 'accountability-reports',
        name: 'inventory.reports_accountability',
        component: KamaligAccountabilityReports
    },
]

export default {
    inventory_routes: inventory_routes
}