import Vue from 'vue'
import VueRouter from 'vue-router'
import PageBody from '../components/PageBody'
import Login from '../components/Login'
import Biometrics from '../components/Biometrics'
import auth from '../middleware/auth'
import log from '../middleware/log'
import ErrorView from '../views/404'
import AdminRoutes from '../router/admin'
import HumanResourceRoutes from '../router/human_resource'
import InventoryRoutes from '../router/inventory'
import SignUp from '../components/Signup'
import axios from 'axios';
import store from '../store';

Vue.use(VueRouter)
AdminRoutes.admin_routes
let human_routes = HumanResourceRoutes.human_routes
let inventory_routes = InventoryRoutes.inventory_routes

let routes = [
    {
        path: '/signup/:token',
        name: 'signup',
        component: SignUp
    },
    {
        path: '/biometrics',
        name: 'biometrics',
        component: Biometrics,
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        beforeEnter: (to, from, next) => {
            console.log(to);
            if (localStorage.getItem('token') !== null) next({name: 'home'})
            else next()
        },
    },
    {
        path : '/administrator',
        component: PageBody,
        children: AdminRoutes.admin_routes,
        meta: {
            middleware: [auth,log]
        }
    },
    {
        path: '/inventory',
        component: PageBody,
        children: inventory_routes
    },
    {
        path: '/human_resource',
        component: PageBody,
        children: human_routes
    },
    {
        path: '/error',
        component: PageBody,
        children: [
            {
                path: '/not-permitted',
                name: 'permission-error',
                component: ErrorView
            }
        ]
    },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
    store.dispatch('fetchLoader', true);
    if(to.name == 'biometrics' && localStorage.getItem('token') === null && to.name !== 'signup')
    {
        next();
    }
    else if (to.name !== 'login' && localStorage.getItem('token') === null && to.name !== 'signup') 
    {
        next({name: 'login'})
    }
    else {
        console.log(to)
        let ignore = [
            'signup',
            'administrator',
            'login',
            'biometrics',
            'home',
            'admin.home',
            'human.home',
            'inventory.home',
            'permission-error',
            'my_portal.view',
            'human_resource.activities',
            'notifications',
            'inventory.reports_assets',
            'human_resource.reports_list',
            'inventory.reports_accountability'
        ];
        
        var length = ignore.length;
        let name_ignored = false;
        for(var i = 0; i < length; i++) {
            if(ignore[i] == to.name){
                name_ignored = true;
                break;
            }
        }
        console.log(!name_ignored);

        if(!name_ignored){
            axios.get('restrictions/'+to.name).then( response => {
                if(response.data.permitted ){
                    next();
                }else{
                    next({
                        name: 'permission-error'
                    });
                }
            });
        }else{
            next();
        }
    }

});

router.afterEach(() => {
    store.dispatch('fetchLoading', false);
    store.dispatch('fetchLoader', false);
    console.log('loading ' + store.getters.loading);
});

export default router
