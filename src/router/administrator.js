import AdminDashboard from '../views/administrator/Dashboard'

export default [
    {
        path: '/',
        redirect: 'administrator',
        name: 'home',
        component: AdminDashboard
    },
    {
        path: '/administrator',
        name: 'administrator',
        component: AdminDashboard
    }

]