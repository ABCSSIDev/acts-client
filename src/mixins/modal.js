export const modalMixins = {
    data(){
        return {
            modalData:{
                title: '',
                component: '',
                data:'',
            }
        }
    },
    methods: {
        showModal(event, data = null) {
            this.modalData.title = event.currentTarget.getAttribute('title')
            this.modalData.component = event.currentTarget.getAttribute('component')
            this.modalData.data = data
            this.$bvModal.show('modal-scoped')
            this.$root.$emit('set-title',this.modalData)
        },
        deleteModal(event,data) {
            this.modalData.title = event.currentTarget.getAttribute('title')
            this.modalData.component = event.currentTarget.getAttribute('component')
            this.modalData.data = data
            this.$bvModal.show('modal-scoped')
            this.$root.$emit('set-title',this.modalData)
        },
    }
};