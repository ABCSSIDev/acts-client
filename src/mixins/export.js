import Papa from 'papaparse'
export const exportData = {
    methods: {
        dataExport: function(object,filename,extension){
            let blob = new Blob([Papa.unparse(object)],{type: 'text/csv;charset=utf-8;'})
            let url = URL.createObjectURL(blob)

            let link = document.createElement("a");
            link.setAttribute("href", url);
            link.setAttribute("download", filename+'.'+extension);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click()

            document.body.removeChild(link)
            return true
        },
        dataImport: function(csv, template_headers){
            let res_arr = [];
            let missing = "";
            let validation_error = [];
            Papa.parse(csv,{
                skipEmptyLines: true,
                complete: function(results){
                    let header = results.data[0]//headers in excel
                    let fields = [];
                    header.forEach(value => {
                        let required = false
                        if(value.charAt(value.length-1) == "*"){
                            required = true
                        }
                        fields.push(required);
                    });
                    let temp_header = Object.keys(template_headers);
                    temp_header.forEach(temp_head => {
                        if(!header.includes(temp_head)){
                            missing += temp_head;
                        }
                    })
                    if(missing == ""){
                        for (let index = 1; index < results.data.length; index++) {
                            const element = results.data[index];
                            let object = {}
                            for(let x in header){
                                if(fields[x]){
                                    let valid = false;
                                    if(element[x] == ""){
                                        valid = true;
                                    }
                                    validation_error.push(valid);
                                }
                                object[header[x]] = element[x]
                            }
                            res_arr = res_arr.concat(object)
                        }
                    }
                }
            })
            return {
                "missing_fields":  validation_error,
                "missing_header": missing,
                "contents": res_arr
            };
        }
    }
}