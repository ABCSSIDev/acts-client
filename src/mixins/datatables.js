import Vuetable from 'vuetable-2'
import VuetableFieldHandle from 'vuetable-2/src/components/VuetableFieldHandle'
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination'
import _ from 'lodash'
import axios from 'axios'

export default {

    mounted() {
        axios.get('system').then( response => {
            this.data = response.data
        })
    }

}