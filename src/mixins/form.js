import { validationMixin } from "vuelidate";
import axios from 'axios'
import { mapActions } from 'vuex'

export const formMixins = {
    mixins:[validationMixin],
    methods: {
        ...mapActions({
            fetchLoader: 'fetchLoader'
        }),
        formReset(form){
            for (let val in form.$model) {
                this.$set(form.$model,val,null)
            }
            this.$nextTick(() => {
                this.$v.$reset();
            });
            return form.$model
        },
        editForm(route, 
            additionals = { stay: false, route: null}, 
            arrayRoute = null, 
            arrayData = null
        ) {
            this.fetchLoader(true);
            this.$v.form.$touch()
            if (this.$v.form.$anyError){
                this.fetchLoader(false)
                this.$bvModal.hide('modal-main') //close modal active
                this.makeToast('Oopps!', 'Fields with red are required', 'danger')
                return
            }

            try {
                let form_data = this.$v.form.$model;
                return axios.put(route, form_data).then((response) => {
                    if(arrayRoute != null && arrayData != null ) {
                        arrayData.forEach((array) => {
                            if(!array.id) {
                                axios.post(route + arrayRoute, array)
                            }
                            axios.put(route + arrayRoute + '/' + array.id,array)
                        });  
                    }
                    
                    let data = response.data;
                    this.fetchLoader(false);
                    this.$bvModal.hide('modal-main') //close modal active
                    if (data.error){
                        this.makeToast('Oopps!',data.desc,'danger')
                    } else {
                        this.makeToast('Success','Successfully Edited','success')
                        if (additionals.stay) {
                            this.formReset(this.$v.form)
                        } else {
                            this.$router.push({
                                name: additionals.route,
                                params: { id: data.id } 
                            });
                        }
                    }
                    return response.data
                });
            } catch (e) {
                this.fetchLoader(false)
            }
        },
        submitForm(route, additionals = { stay: false, route: null, edit: false }, arrayRoute = null, arrayData = null){
            this.fetchLoader(true);
            this.$v.form.$touch()
            if (this.$v.form.$anyError){
                this.fetchLoader(false)
                this.$bvModal.hide('modal-main') //close modal active
                this.makeToast('Oopps!','Fields with red are required','danger')
                return
            }
            try {
                let form_data = this.$v.form.$model;
                if(additionals.edit){
                    form_data._method = "PUT"
                }

                return axios.post(route,form_data).then((response) => {
                    if(arrayRoute != null) {
                        arrayData.forEach((array) => {
                            axios.post(route + '/' + response.data.id + arrayRoute,array)
                        });   
                    } 
                    let data = response.data;
                    this.fetchLoader(false);
                    this.$bvModal.hide('modal-main') //close modal active
                    if (data.error){
                        this.makeToast('Oopps!',data.desc,'danger')
                    } else {
                        let toast_message = additionals.edit?'Successfully Edited':'Successfully Created';
                        this.makeToast('Success',toast_message,'success')
                        if(additionals.stay){
                            this.formReset(this.$v.form)
                        } else {
                            this.$router.push({
                                name: additionals.route,
                                params: { id: data.id } 
                            });
                        }
                        this.checked = false;
                    }
                    return response.data
                    
                })
            }catch (e) {
                this.fetchLoader(false)
                console.log(e)
            }

        },
        deleteForm(route, id, route2){
            axios.delete(route + id).then((response) => {
                let data = response.data
                if (data.error){
                    this.makeToast('Oopps!',data.desc,'danger')
                } else {
                    this.makeToast('Success','Successfully Deleted','success')
                    this.$router.push({name: route2})
                    this.$bvModal.hide('modal-scoped')
                }
            });
        },
        deleteMassForm(route,id){
            axios.delete(route+id).then((response) => {
                let data = response.data
                if (data.error){
                    this.makeToast('Oopps!',data.desc,'danger')
                } else {
                    this.makeToast('Success','Successfully Deleted','success')
                    this.$bvModal.hide('modal-main')
                }
            });
        },
        stateValidation(name, additionals =  {child: false, index: null, name: null}){
            const { $dirty, $error } = additionals.child?this.$v.form[name].$each[additionals.index][additionals.name]:this.$v.form[name];
            return $dirty ? !$error : null;
        },
        stateValidationItems(name){
            const { $dirty, $error } = this.$v.formitems[name];
            return $dirty ? !$error : null;
        },
        makeToast(title, message, variant = null){
            this.$bvToast.toast(message,{
                title: title,
                variant: variant,
                solid: true
            })
        },
        validateState(name, form){
            let dirty = form[name].$dirty
            let error = form[name].$error
            // let { $dirty, $error } = form[name];
            return dirty ? !error : null;
        },
        submitPayrollForm(route, form_data, $route) {
            this.fetchLoader(true);
            try {
                return axios.post(route,form_data).then((response) => {
                    this.fetchLoader(false);
                    let data = response.data
                    if (data.error){
                        this.makeToast('Oopps!',data.desc,'danger')
                    } else {
                        let toast_message = 'Successfully Created';
                        this.makeToast('Success',toast_message,'success')

                        this.$router.push({
                            name: $route,
                            params: { id: data.id } 
                        });
                    }
                    return response.data
                    
                })
            }catch (e) {
                this.fetchLoader(false)
                console.log(e)
            }
        }

    }
}